#ifndef APPROJECT_FUNCTIONS_H
#define APPROJECT_FUNCTIONS_H

#include <iostream>
#include <string>
#include <cmath>
#include <tuple>
#include <ctime>
#include "item.h"
#include "input.h"
using namespace std;

// decimal to binary function for finding cromosoms
string decToBin(int dec, const int &resLength)
{
    string result = "";
    int cnt = 0;
    while (dec > 0)
    {
        if (cnt > resLength)
            return result;
        result += char((dec % 2) + 48);
        dec /= 2;
        cnt++;
    }
    while (cnt < resLength)
    {
        result += "0";
        cnt++;
    }
    return result;
}

// fitness function
tuple<int, int> fitness(const string &cromosom)
{
    int sVal = 0;
    int sWei = 0;

    for (int i = 0; i < cromosom.length(); i++)
    {
        if (cromosom[i] == '1')
        {
            sVal += items[i].get_value();
            sWei += items[i].get_weight();
            if (sWei > maxW)
                // heavy
                return make_tuple(-1, -1);
        }
    }
    return make_tuple(sWei, sVal);
}

// selection function
tuple<int, int> selection(const long long int &populationLength, const int fitnessList[])
{

    int s1 = rand() % (populationLength);
    int s2 = rand() % (populationLength);
    if (fitnessList[s1] == -1)
    {
        while (fitnessList[s1] == -1)
        {
            s1 = rand() % (populationLength);
        }
    }

    if (fitnessList[s2] == -1 || s1 == s2)
    {
        while (fitnessList[s2] == -1 || s1 == s2)
        {
            s2 = rand() % (populationLength);
        }
    }
    return make_tuple(s1, s2);
}

// crossover function
tuple<string, string> crossover(const int &firstS, const int &secondS, const string population[])
{
    string temp1 = "";
    string temp2 = "";
    temp1 = population[firstS].substr(numberOfItems / 3);
    temp2 = population[secondS].substr(numberOfItems / 3);
    return make_tuple(population[firstS].substr(0, numberOfItems / 3) + temp2,
                      population[secondS].substr(0, numberOfItems / 3) + temp1);
}

// mutation
void mutation(string &selectedCoromosom)
{
    int selectedGen = rand() % numberOfItems;
    selectedCoromosom[selectedGen] = (selectedCoromosom[selectedGen] == '1' ? '0' : '1');
}

#endif // APPROJECT_FUNCTIONS_H
