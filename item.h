#ifndef APPROJECT_ITEM_H
#define APPROJECT_ITEM_H
// each item class represent an item with a value and a weight
class item
{
    int value;
    int weight;
public:
    item(int wei, int val): weight(wei), value(val) {}
    int get_value() {return value;}
    int get_weight() {return weight;}
};
#endif
