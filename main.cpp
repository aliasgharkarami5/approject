// in The name of GOD

#include "functions.h"
#include "iomanip"

using namespace std;

int main()
{
	cout<<"code started please wait until the result ...\n\n";
    srand(time(0));

    // generating population - list of all possible ways
    long long int populationLength = (long int)pow(2, numberOfItems);

    // memory allocation for higher numbers
    string *population = new string[populationLength];
    int *fitnessList = new int[populationLength];
    int *weightList = new int[populationLength];

    // generation 0
    for (int i = 0; i < populationLength; i++)
    {
        population[i] = decToBin(i, numberOfItems);
        tie(weightList[i], fitnessList[i]) = fitness(population[i]);
    }

    // loop until best answer - other generations
    // saving generations just for fun
    int generation = 0;
    long long int lengthCheck = populationLength;

    // vars
    //  selection and crossover
    int firstSelected, secondSelected;
    string temp1, temp2;
    // mutation
    int selectedForMutation;
    // maxFitness
    int maxFitness = -1;
    // handeling equal results
    long long int repeatingResult = -1;

    while (lengthCheck > 1)
    {
        // selection and crossover
        // here we will select random cromosoms and doing crossovers on them
        // we will select only populationLength / number of items pairs
        for (int i = 0; i < (populationLength / numberOfItems); i++)
        {
            tie(firstSelected, secondSelected) = selection(populationLength, fitnessList);
            // crossover part
            tie(population[firstSelected], population[secondSelected]) = crossover(firstSelected, secondSelected, population);
            tie(weightList[firstSelected], fitnessList[firstSelected]) = fitness(population[firstSelected]);
            tie(weightList[secondSelected], fitnessList[secondSelected]) = fitness(population[secondSelected]);
        }
        // mutation
        for (int i = 0; i < (populationLength / numberOfItems); i++)
        {
            selectedForMutation = rand() % populationLength;
            mutation(population[selectedForMutation]);
            tie(weightList[selectedForMutation], fitnessList[selectedForMutation]) = fitness(population[selectedForMutation]);
        }

        generation += 1;
        // cout << "generation " << generation << endl;

        // loopBreaker
        lengthCheck = 0;
        for (int i = 0; i < populationLength; i++)
        {
            if (fitnessList[i] > maxFitness)
            {
                lengthCheck += 1;

                // debug
                //  cout << fitnessList[i] << '\t' << weightList[i] << '\t' << population[i] << endl;
            }
        }
        // debug
		// cout << lengthCheck << endl;

        // upgrading the newer generations by removing its weaknesses
        // in some cases it will jump over the answer and say we have no answer , this part will force it to check the next generations
        // the jump may result cause of the equal combinations for example 110 and 110 have equal weight and height and it will cause loop it will be handled in line  with comment *
        if (repeatingResult == 0 && lengthCheck != 0)
        {
            repeatingResult = lengthCheck;
        }
        else if (repeatingResult == lengthCheck && lengthCheck != 0)
        {
            break;
        }

        if (lengthCheck < 1)
        {
            if (repeatingResult == -1)
            {
                // by 0 we are activating it (*)
                repeatingResult = 0;
            }
            // maxFitness-=1 to make it check the last fitnesses again
            maxFitness -= 1;
            // forcing the loop to evolve
            lengthCheck = 2;
        }
        else
        {
            // this is the part it will increase the limitations to speed up finding the answer ( app will not find answer without this limitations )

            // if lengthCheck is higher than 100 it will evolve faster

            if (lengthCheck > 100)
            {
                maxFitness += numberOfItems;
            }
            else
            {
                maxFitness += 1;
            }
        }
    }

    // print result
    cout << "the answer with "<<numberOfItems<<" items and maximum weight "<< maxW <<" found after " << generation << " generations and it is : " << endl;
    cout << "weight" << setw(10) << "value" << setw(18) << "combination" << endl;
    for (int i = 0; i < populationLength; i++)
    {
        // >= instead of > because it was igonoring the answer
        if (fitnessList[i] >= maxFitness)
        {
            cout << fitnessList[i] << setw(12) << weightList[i] << setw(30) << population[i] << endl;
        }
    }
    delete[] population;
    delete[] fitnessList;
    delete[] weightList;
    return (0);
}

// ba tashakor az ostad mohammadi va ostad bakhtiari