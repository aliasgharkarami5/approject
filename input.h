#ifndef APPROJECT_INPUT_H
#define APPROJECT_INPUT_H
#include "item.h"
// change this part as inputs *********************************************

int numberOfItems = 20;

// list of items we have
item items[] = { item(1, 7), item(1, 1), item(7, 7), item(6, 2), item(2, 5), item(4, 10), item(3, 7), item(1, 2), item(8, 2), item(1, 10), item(7, 14), item(5, 10), item(1, 10), item(2, 10), item(7, 14), item(5, 10), item(20, 20), item(4, 8), item(4, 5), item(12, 5)};

// maximum weight our knapsack can handle
int maxW = 20;

//*************************************************************************
#endif
